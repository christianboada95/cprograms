//
//  main.cpp
//  Robot_Limpiador
//
//  Created by Cristian Boada on 25/11/19.
//  Copyright © 2019 Cristian Boada. All rights reserved.
//

// El robot impiador se hunde en la pscina y una vez llega al fondo calculasu posición respecto a los bordes
// de la pscina. Para ello el robot digitaliza la pscina mediante una matriz de MxN cuyos elementos valen 0
// si aún está sucia y 1 zona limpiada.
// Se desea hacer un algoritmo que dada una posicion inicial cualquiera, el robot limpie toda la piscina
// sin pasar dos veces por el mismo sitio.

#include <iostream>
#include <stdlib.h>

using namespace std;

const int MAX = 50;

void IniciarMatriz(int M[][MAX], int, int);
void MostrarMatriz(int M[][MAX], int, int);
bool MatrizLimpia(int M[][MAX], int, int);
void Limpiar(int M[][MAX], int, int, int x, int y);
void CambiarDireccion(int M[][MAX], int, int, int x, int y, int &dx, int &dy);

int main(int argc, const char * argv[]) {
    // TamaÒo de la matriz
    int n = 6;//21;
    int m = 6;//21;
    
    cout << "Ingresa el tamaÒo de la matriz n(max 50): ";
    cin >> n;
    cout << "Ingresa el tamaÒo de la matriz m(max 50): ";
    cin >> m;
    
    int puntos[n][MAX];
    
    // punto inicial
    int x=2, y=2;
    cout << "Ingresa el valor inicial para x: ";
    cin >> x;
    cout << "Ingresa el valor inicial para y: ";
    cin >> y;
    
    IniciarMatriz(puntos, n, m);
    MostrarMatriz(puntos, n, m);
    
    Limpiar(puntos, n, m, x, y);
    
}

void Limpiar(int M[][MAX], int col, int row, int x, int y) {
    
    int dy = 0;
    int dx = 0;
    while(!MatrizLimpia(M, col, row)){
        //while(M[y+dy][x+dx] == 0 and (y+dy < row and x+dx < col)){
        M[y+dy][x+dx] = 1;
        y += dy;
        x += dx;
        //}
        
        MostrarMatriz(M, col, row);
        cout << "dir: (" << dy << " - " << dx << ")" << endl;
        cout << "pos: (" << y << " - " << x << ")" << endl;
        
        CambiarDireccion(M, col, row, x, y, dx, dy);
        system("pause");
    }
    
}

void CambiarDireccion(int M[][MAX], int col, int row, int x, int y, int &dx, int &dy) {
    
    if(M[y+1][x] == 0 and y+1 < row) { // 0 1
        dx = 0;
        dy = 1;
    } else if(M[y][x+1] == 0 and x+1 < col) { // 1 0
        dx = 1;
        dy = 0;
    } else if(M[y][x-1] == 0 and x-1 >= 0) { // -1 0
        dx = -1;
        dy = 0;
    } else if(M[y-1][x] == 0 and y-1 >= 0) { // 0 -1
        dx = 0;
        dy = -1;
    }
}

bool MatrizLimpia(int M[][MAX], int col, int row) {
    
    for(int i=0; i<row; i++){
        for(int j=0; j<col; j++){
            if(M[i][j] == 0)
                return false;
        }
    }
    
    return true;
}

void IniciarMatriz(int M[][MAX], int col, int row) {
    
    for(int i=0; i<row; i++){
        for(int j=0; j<col; j++){
            M[i][j] = 0;
        }
    }
}

void MostrarMatriz(int M[][MAX], int col, int row) {
    system("CLS");
    for(int i=0; i<row; i++){
        for(int j=0; j<col; j++){
            if (M[i][j])
                cout << "x ";
            else
                cout << "o ";
        }
        cout << endl;
    }
}

