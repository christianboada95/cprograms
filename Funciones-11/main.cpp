//
//  main.cpp
//  Funciones-11
//
//  Created by Cristian Boada on 22/10/19.
//  Copyright © 2019 Cristian Boada. All rights reserved.
//

//Una empresa tiene sus productos codificados mediante referencias compuestas por 4 cifras. Las dos primeras cifras representan un consecutivo del producto, las siguientes dos cifras representan la línea de producción a la que pertenece (por ejemplo la referencia 7211 está asignada a un producto cuyo consecutivo es 72 y pertenece a la línea 11). En la empresa existen solo tres líneas de producción 11, 22 y 44. Elabore un programa en C que lea n referencias validas de productos (n dado por teclado), sus costos de producción y precios de venta e indique cual es el producto que más ganancia le genera a la empresa. Para ello debe implementar las siguientes funciones:
//- Una función que lea n referencias de productos, sus costos de producción y su valor de venta de cada uno y retorne la referencia del producto que más ganancia le genera a la empresa.
//- Una función que verifique si se trata de una referencia valida y retorne 1 si es válida y 0 si no lo es.
//- Una función que reciba la referencia de cada producto, su costo de producción y su valor de venta y retorne el valor que aporta de ganancia dicho producto.
//- La función main


#include <iostream>

using namespace std;

int referencias();
bool verificarCodigo(int cod);
int gananciaProducto(int cod, int costo, int valor);
int digitos(int);

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Ejerccio 11 del taller de funciones\n";
    
    int referencia = referencias();
    cout << "referencia de producto de mayor ganancia: " << referencia << endl;
    
    return 0;
}

int digitos(int num) {
    // Cuenta cuantos cifras tiene un numero
    int cont = 0;
    while (num > 0) {
        num = num/10;
        cont++;
    }
    
    return cont;
}

bool verificarCodigo(int cod) {
    
    if (digitos(cod) == 4) {            // todos los codigos deben tener 4 cifras
        int consecutivo = cod/100;      // divido entre 100 para sacar los primeros dos digitos
        int linea = cod%100;            // modulo entre 100 para sacar los ultimos 4 digitos
        if (linea == 11 or linea == 22 or linea == 44) {
            return true;
        } else
            return false;
    } else
        return false;
}

int gananciaProducto(int cod, int costo, int valor) {
    // ...
    int ganancia = valor - costo;
    return ganancia;
}

int referencias() {
    // ...
    int n = 0;
    cout << "Ingrese la cantidad de productos que desea registrar: ";
    cin >> n;
    
    int gananciaMayor = 0;
    int referencia = 0;             // Referencia con mayor ganancia
    int cont = 0;
    while (cont < n) {
        int cod = 0;
        int costo = 0;
        int valor = 0;
        cout << "\nIngrese la referencia del producto " << cont << " (4 digitos): ";
        cin >> cod;
        
        // Si el código es valido continúa
        if (verificarCodigo(cod)) {
            cout << "\nIngrese el costo de produccion " << cont << " : ";
            cin >> costo;
            cout << "\nIngrese la valor de venta " << cont << " : ";
            cin >> valor;
            
            // si la ganancia del producto actual es mayor
            if (gananciaProducto(cod, costo, valor) > gananciaMayor) {
                referencia = cod;               // guardo el nuevo código de referencia
                gananciaMayor = gananciaProducto(cod, costo, valor);
            }
            
            cont++;
        } else { // sino, se repite la iteración
            cout << "\nCodigo no valido" << endl;
        }
        
        
    }
    
    return referencia;
}
