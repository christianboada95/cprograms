//
//  main.cpp
//  BuscandoN
//
//  Created by Cristian Boada on 28/10/19.
//  Copyright © 2019 David Mendoza Mejia. All rights reserved.
//

#include <iostream>
#include <locale.h>
using namespace std;

/**
 Hacer una array unidimensional que acepte ocho n˙meros enteros.
 Luego le pregunte al usuario que ingrese un n˙mero a buscar, implementar una función que busque ese número,
 si lo encuentra, debe retornar un valor de verdaderos, en caso contrario, retornar falso
 */

void findN (int array [], int cant);

int main (){
    setlocale (LC_ALL,"");
    
    int tam = 8;
    int vector [tam];
    
    
    for (int i = 0 ; i < tam ; i++)
    {
        cout << "ingrese el valor " << i + 1 << " : ";
        cin >> vector [i];
    }
    
    findN (vector, tam);
    
}

void findN (int array [], int cant)
{
    int n;
    int cont1 = 0;
    cout << "Ingresar  el n˙mero que se va a buscar: ";
    cin >> n;
    
    cout << endl;
    
    for (int i = 0 ; i < cant ; i++)
    {
        if (n == array [i])
        {
            cont1=1;
            
            i = cant;
        }
        
    }
    if (cont1 == 1)
    {
        cout << "verdadero";
    }
    else
    {
        cout << "falso";
    }
    
    
}
